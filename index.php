<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>s02: Activity</title>
</head>
<body>
	<h1>Activity</h1>
	<h3>Divisible of 5</h3>
	<?php printDivisibleOfFive() ;?>

	<h3>Student Array</h3>

	<h4>Current Number of Students</h4>
	<pre><?php echo count($studentNames) ;?></pre>

	<h4>Added Student 1</h4>
	<?php array_push($studentNames, 'Mojo') ;?>
	<pre><?php print_r($studentNames) ;?></pre>
	
	<h4>Printing the New Count</h4>
	<pre><?php echo count($studentNames) ;?></pre>

	<h4>Added Student 2</h4>
	<?php array_push($studentNames, 'Magic') ;?>
	<pre><?php print_r($studentNames) ;?></pre>

	<h4>Printing the New Count</h4>
	<pre><?php echo count($studentNames) ;?></pre>

	<h4>Added Student 3</h4>
	<?php array_push($studentNames, 'Egypt') ;?>
	<pre><?php print_r($studentNames) ;?></pre>

	<h4>Printing the New Count</h4>
	<pre><?php echo count($studentNames) ;?></pre>

	<h4> Remove first student</h4>

	 <?php array_shift($studentNames) ;?>
	 <pre><?php print_r($studentNames) ;?></pre>

	<h4>Printing the New Count</h4>
	 <pre><?php echo count($studentNames) ;?></pre>

	
</body>
</html>